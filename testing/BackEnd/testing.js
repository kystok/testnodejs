const User = require('../../db/models/user.js'),
    Post = require('../../db/models/post.js'),
    assert = require('chai').assert,
    describe = require('mocha').describe,
    mongoose = require('mongoose'),
    db = mongoose.createConnection("mongodb://localhost:27017/test"),
    api = require('../../db/api');

describe('Пользователи', () => {
    it('Создание пользователя', async () => {
        const username = '1testingUserName1',
            r = await User.find({username}).remove().exec(),
            password = '1234',
            result = await api.createUser({username, password});
        assert.equal(result.status === 'ok', true);
    });
});

describe('Посты', () => {
    let id = [];
    it('Создание поста', async () => {
        for (let i = 0; i < 10; i++) {
            let header = "Заголовок" + i,
                text = "текст" + i,
                result = await api.createPost({text, header});
            assert.equal(result.status === 'ok', true);
            id.push(result.data._id);
        }
    });
    it('Редактирование поста', async () => {
        const header = "Заголовок-1",
            text = "new text",
            result = await api.editPost({text, header, id: id[0]});
        assert.equal(result.status === 'ok', true);
    });
    it('Получение полного списка постов', async () => {
        const result = await api.getPosts();
        assert.equal(result.status === 'ok', true);
    });
    it('Получение списка всех постов (пагинация)', async () => {
        const count = 5,
            result = await api.getPagPosts({count});
        assert.equal(result.status === 'ok', true);
    });
    it('Получение списка отдельных постов (пагинация)', async () => {
        const count = 2,
            number = 3,
            result = await api.getPagPost({count, number});
        assert.equal(result.status === 'ok', true);
    });
    it('Очистка после тестирования', async () => {
        id.forEach((id) => {
            Post.find({_id: id}).remove().exec();
        });
    });

});

