"use strict";
let mongoose = require('mongoose'),
    crypto = require('crypto'),
    db = mongoose.connect("mongodb://localhost:27017/test"),
    User = require('./models/user.js'),
    Post = require('./models/post.js');

/**
 * Создание пользователя
 * @param {String} username - имя пользователя
 * @param {String}password - пароль пользователя
 * @return {Object} status - ok/error, data - информация о пользователе
 */
async function createUser({username=null, password=null}) {
    if (username && password) {
        let user = new User({
            username: username,
            password: hash(password),
            access: 1
        });

        let r = await User.findOne({"username": username});
        if (r) return {status: 'error', error: 'Пользователь уже существует'};
        await user.save((err) => {

            if (err) return {status: 'error'};
        });
        user.password = undefined;
        return {status: 'ok', data: user};
    } else return {status: 'error'};
}

/**
 * Вход
 * @param {String} username - имя пользователя
 * @param {String} password - пароль пользователя
 * @return {Object} status - ok/error, data - информация о пользователе
 */
async function checkUser({username=null, password=null}) {
    if (username && password) {
        console.log(username && password)
        let user = await User.findOne({username: username});
        if (user) {
            if (user.password === hash(password)) {
                user.password = undefined;
                return {status: 'ok', data: user};
            }
            return {status: 'error', error: 'Неверный логин/ПАРОЛЬ'}
        }
        return {status: 'error', error: 'Неверный ЛОГИН/пароль'}
    }
    else return {status: 'error', error: 'Неверный ЛОГИН/пароль'}
}

/**
 * Создание поста
 * @param {String} header - измененный заголовок,
 * @param {String} text - измененный текст,
 * @param {String} id  - изменяемого поста
 * @return {Object} object status - ok/error, data - информация о посте
 */
async function createPost({text, header}) {
    let post = new Post({
        text: String(text), header: String(header), date_update: new Date(), date: new Date()
    });
    await post.save(async (err, res) => {
        if (err) return {status: 'error'};
    });
    return {status: 'ok', data: post};
}

/**
 * Редактирование поста
 * @param {String} text, header - измененные данны,
 * @param {String} - id изменяемого поста
 * @return {Object} status: ok/error, data - информация о посте
 */
async function editPost({text, header, id}) {
    let res = await Post.findOneAndUpdate({_id: String(id)}, {
        text: String(text),
        header: String(header),
        date_update: new Date
    }, (err, res) => {
        if (err) return {status: 'error'};
    });
    return {status: 'ok', data: res};
}

/**
 *   Выгрузка постов (всех)
 *   @return {Object} status,data data: массив из постов
 */
async function getPosts() {
    let res = await Post.find({});
    return {status: 'ok', data: res};
}

/**
 *   Выгрузка постов (всех) + готовая пагинация.
 *   @param {String/Integer} count - количество постов на одной странице
 *   @return {Object} {status,data} data: двумерный массив (размер второго вложенного массива = count)
 */
async function getPagPosts({count}) {
    let _count = +count,
        _res = [],
        tmp = [],
        res = await Post.find({});
    if (res.length > 0)
        res.forEach((data, i) => {
            tmp.push(data);
            if ((i + 1) % _count === 0) {
                _res.push(tmp);
                tmp = []
            }
        });
    return {status: 'ok', data: _res};
}

/**
 *   Выгрузка постов по пагинации, для выбранной страницы.
 *   @param {String/Integer} count - количество постов на одной странице
 *   @param {String/Integer} number - номер страницы
 *   @return {Object} {status,data} data: массив постов
 */
async function getPagPost({count, number}) {
    let _count = +count,
        _number = +number,
        skip = (_number - 1) * _count;
    if (skip >= 0 && _count > 0) {
        let res = await Post.find({}).skip(skip).limit(_count);
        return {status: 'ok', data: res};
    } else {
        return {status: 'error', data: "Проверьте входные данные"};
    }

}


/**
 *  хеш функция
 *  @param {String} text - пароль
 *  @return {String} хеш пароля
 */
function hash(text) {
    return crypto.createHash('sha1')
        .update(text).digest('base64')
}


module.exports = {
    checkUser: checkUser,
    createUser: createUser,
    hash: hash,
    editPost: editPost,
    getPagPost: getPagPost,
    getPosts: getPosts,
    getPagPosts: getPagPosts,
    createPost: createPost
};