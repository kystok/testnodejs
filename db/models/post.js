let mongoose = require('mongoose'),
    Post = new mongoose.Schema({
            header: {
                type: String,
                unique: false,
                required: true
            },
            text: {
                type: String,
                unique: false,
                required: true
            },
            date: {
                type: Date,
                required: true
            },
            date_update: {
                type: Date,
                required: true
            },
        },
        {versionKey: false});
module.exports = mongoose.model('Post', Post);