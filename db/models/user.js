let mongoose = require('mongoose'),
    User = new mongoose.Schema({
            username: {
                type: String,
                unique: true,
                required: true
            },
            password: {
                type: String,
                required: true
            },
            access: {
                type: Number,
                default: 1
            }
        },
        {versionKey: false});
module.exports = mongoose.model('User', User);