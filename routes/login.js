'use strict';
let express = require('express'),
    router = express.Router(),
    api = require('../db/api');

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

router.post('/', async (req, res, next) => {
    if (req.body) {
        let username = req.body.username || null,
            password = req.body.password || null;

        let user = await api.checkUser({username, password});
        (user.status === 'ok') ? req.session.user = user.data : res.send({user});
        res.send(req.session.user)

    }  res.send("Упс")
});


module.exports = router;
