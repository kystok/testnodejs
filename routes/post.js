'use strict';
let express = require('express'),
    router = express.Router(),
    db = require('../db/api');

/* GET users listing. */
router.use('/', function (req, res, next) {
    res.send('404 Not Found');
});
router.post('/edit', async (req, res, next) => {
    switch (checkSession(req.session)) {
        case 1:
            res.send('Нет доступа для пользоватея!');
            break;
        case 0:
            let text = req.body.text || null,
                header = req.body.header || null,
                id = req.body.id || null,
                result = await db.editPost({text,header,id});
            res.send(result);
            break;
        default:
            res.send("Нет доступа для гостя");
            break;
    }
});

router.post('/getAll', async (req, res, next) => {
    switch (checkSession(req.session)) {
        case 0:
        case 1:
            let result = await db.getPosts();
            res.send(result);
            break;
        default:
            res.send("Нет доступа для гостя");
            break;
    }
});

router.post('/getPag', async (req, res, next) => {
    switch (checkSession(req.session)) {
        case 1:
            res.send('Нет доступа для пользоватея!');
            break;
        case 0:
            let count = req.body.count || null,
                number = req.body.number || null,
                result = await db.getPagPost({count, number});
            res.send(result);
            break;
        default:
            res.send("Нет доступа для гостя");
            break;
    }
});

router.post('/create', async (req, res, next) => {
    switch (checkSession(req.session)) {
        case 1:
            res.send('Нет доступа для пользоватея!');
            break;
        case 0:
            let text = req.body.text || null,
                header = req.body.header || null,
                result = await db.createPost({text, header});
            res.send(result);
            break;
        default:
            res.send("Нет доступа для гостя");
            break;
    }
});


function checkSession(session) {
    if (!!session.user) {
        if (session.user.access === 0) {
            return 0;
        } else {
            return 1;
        }
    }
    else {
        return -1;
    }
}

module.exports = router;
