'use strict';
let express = require('express'),
    router = express.Router(),
    db = require('../db/api');

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});


router.post('/', async (req, res, next)=> {
    if (!!req.session.user) {
        if (req.session.user.access === 0) {
            let {username, password} = req.body;
            let user = await db.createUser({username, password});
            if (user.status === 'ok'){
                delete user.data.password;
                res.send(user);  //ok
            }
            res.send(`Что-то пошло не так, ${user.error}`);
        } else {
            res.send('Страница холопа')
        }
    }
    else {
        res.send("Гость")
    }
});


module.exports = router;
