'Use strict'
let express = require('express'),
    path = require('path'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    newUser = require('./routes/user'),
    login = require('./routes/login'),
    post = require('./routes/post'),
    app = express();


const MongoStore = require('connect-mongo')(session);
app.use((req,res,next)=>{
    res.setHeader('Access-Control-Allow-Origin', '*');
    next();
});
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session(
    {
        secret: 'blouJob',
        cookie: {
            httpOnly: true,
            maxAge: null
        },
        store: new MongoStore({
            host: '127.0.0.1',
            port: '27017',
            db: 'test',
            url: 'mongodb://localhost:27017/test'
        }),
        resave: true,
        saveUninitialized: true
    }
));


app.use('/newUser', newUser);
app.use('/login', login);
app.use('/post', post);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
